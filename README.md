## Notion Electron Snap

The following project creates a snap application for notion, all this with the help
of electron.

This is just for educational purposes


## Install dependencies


```npm install```

## Build
```bash
npm run dist 
```

After that, the files are on the folder `dist`, go to the folder `dist` to install



## Install

Note: this is not recommended, it just for educational purposes

The flag dangerous is  necessary because this package does not come from any official snap's repository
```bash
cd dist
sudo snap install --dangerous ./notion-<name>.snap
```
